# STI Semantic Tourist Info #

This teams final project assignment, was to create a mobile application, which shows it’s users information related to touristic activities or locations. The information that is shown to the user comes from an online api, which is the final assignment of another team. The following  parameters can be supplied to the api to get more specific information: the type of activity or location, the minimal and maximum price of an offer, the starting and end date of an offer or event and the location of the user. This data is transmitted in the JSON-LD format and then loaded into the application for further processing.
The application allows the user to select, which of the given parameters are to be used in the information retrieval process and the following showing of the information to the user. The user can change the selection and refresh the data shown at any time. The location information is based on the users coordinates, collected via the Network or GPS location service of the phone.
The offers will be displayed in a simple list, in which the most basic and important Informations are shown. Every offer can then be selected to show more detailed informations. 
It is important to keep in mind, that all offers and their informations come from open source databases or are directly crawled from different webpages. Therefore the application will display only those offers which have a minimum set of data. 

### Download the app ###
Go to Downloads section on the left side to download the apk

### Used Libraries ###
https://github.com/anothem/android-range-seek-bar

https://github.com/square/picasso

https://github.com/sbrunk/jena-android