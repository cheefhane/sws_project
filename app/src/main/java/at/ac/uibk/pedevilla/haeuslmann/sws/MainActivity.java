package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import mobi.seus.org.apache.http.client.methods.HttpGet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String REST_BASE_URL = "http://sws.tr1k.de/";
    public static final String REST_ACTION = "THIS_IS_A_UNIQUE_KEY_WE_USE_TO_COMMUNICATE";

    public DataParser dataParser;
    public int intentCounter = 0;
    public Filter filter;
    private RecyclerView recList;
    private String languageCode;
    private ProgressDialog progress;
    private EditText fromDateEtxt;
    private EditText toDateEtxt;
    private java.text.DateFormat dateFormatter;
    private AlertDialog filterTypesDialog;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private TextView priceLabel;
    private RangeSeekBar<Integer> rangeSeekBar;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intentCounter == filter.getTypes().size()) {
                displayContent();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languageCode =  Locale.getDefault().getLanguage();
        System.out.println("Language: " + languageCode);
        if (!languageCode.equals("nl") && !languageCode.equals("de") && !languageCode.equals("it")) {
            languageCode = "en";
        }

        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dateFormatter = android.text.format.DateFormat.getMediumDateFormat(getApplicationContext());
        filter = new Filter(this);
        setRangeSeekBar();
        setDateTimeField();
        setFilterTypeDialog();

        Button getDataButton = (Button) findViewById(R.id.updateButton);
        if (getDataButton != null) {
        getDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentCounter = 0;
                dataParser = new DataParser(MainActivity.this, languageCode);
                getContent();
            }
        });
        }

        recList = (RecyclerView) findViewById(R.id.cardList);
        if (recList != null) {
            recList.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);
        }
        getContent();
    }

    private void getContent() {
        intentCounter = 0;
        dataParser = new DataParser(this, languageCode);

        progress = ProgressDialog.show(this, "Loading",
                "Please be patient...", true);


        ArrayList<String> endPoints = filter.getEndpoints();
        for (String endPoint : endPoints) {
            try
            {
                HttpGet httpGet = new HttpGet(new URI(REST_BASE_URL + endPoint));
                DataParsingRestTask task = new DataParsingRestTask(this, REST_ACTION);
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, httpGet);
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }

    private void displayContent() {
        List<DAO> dAOList = dataParser.getDaoList();
        System.out.println("DAO List Size before filtering of useless daos: " + dAOList.size());
        ArrayList<DAO> daosToRemove = new ArrayList<>();
        for (DAO dao : dAOList) {
            if (dao.name == null || dao.name.equals("")) {
                daosToRemove.add(dao);
            }
        }
        dAOList.removeAll(daosToRemove);
        System.out.println("DAO List Size after filtering of useless daos: " + dAOList.size());

        DAOAdapter daoAdapter = new DAOAdapter(dAOList, this);
        recList.setAdapter(daoAdapter);
        progress.dismiss();
    }

    private void setRangeSeekBar() {
        rangeSeekBar = (RangeSeekBar<Integer>) findViewById(R.id.priceFilter);
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                filter.setFromPrice(minValue.doubleValue());
                filter.setToPrice(maxValue.doubleValue());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up potentialActionButton, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings3) {
            return true;
        } else if (id == R.id.action_settings4) {
            filterTypesDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.registerReceiver(receiver, new IntentFilter(REST_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(receiver);
    }

    private void findViewsById() {
        fromDateEtxt = (EditText) findViewById(R.id.etxt_fromdate);
        if (fromDateEtxt != null) {
            fromDateEtxt.setInputType(InputType.TYPE_NULL);
        }

        toDateEtxt = (EditText) findViewById(R.id.etxt_todate);
        if (toDateEtxt != null) {
            toDateEtxt.setInputType(InputType.TYPE_NULL);
        }
    }

    private void setDateTimeField() {
        findViewsById();

        fromDateEtxt.setVisibility(View.GONE);
        toDateEtxt.setVisibility(View.GONE);

        fromDateEtxt.setOnClickListener(this);
        toDateEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                filter.setFromDate(newDate.getTime());
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                filter.setToDate(newDate.getTime());
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void setFilterTypeDialog() {

        boolean[] defaultFilters = new boolean[5];
        defaultFilters[2] = true;
        // Arrays.fill(defaultFilters, Boolean.TRUE);
        priceLabel = (TextView) findViewById(R.id.priceLabel);
        priceLabel.setVisibility(View.GONE);
        rangeSeekBar.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Select types to show")
                .setMultiChoiceItems(R.array.types, defaultFilters,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    switch (which) {
                                        case 0:
                                            filter.getTypes().add(DAO.Type.TOURISTATTRACTION);
                                            break;
                                        case 1:
                                            filter.getTypes().add(DAO.Type.EVENT);
                                            fromDateEtxt.setVisibility(View.VISIBLE);
                                            toDateEtxt.setVisibility(View.VISIBLE);
                                            break;
                                        case 2:
                                            filter.getTypes().add(DAO.Type.LODGINGBUSINESS);
                                            break;
                                        case 3:
                                            filter.getTypes().add(DAO.Type.OFFER);
                                            priceLabel.setVisibility(View.VISIBLE);
                                            rangeSeekBar.setVisibility(View.VISIBLE);
                                            break;
                                        case 4:
                                            filter.getTypes().add(DAO.Type.RESTAURANT);
                                            break;
                                    }
                                } else {
                                    switch (which) {
                                        case 0:
                                            filter.remove(DAO.Type.TOURISTATTRACTION);
                                            break;
                                        case 1:
                                            filter.remove(DAO.Type.EVENT);
                                            fromDateEtxt.setVisibility(View.GONE);
                                            toDateEtxt.setVisibility(View.GONE);
                                            break;
                                        case 2:
                                            filter.remove(DAO.Type.LODGINGBUSINESS);
                                            break;
                                        case 3:
                                            filter.remove(DAO.Type.OFFER);
                                            priceLabel.setVisibility(View.GONE);
                                            rangeSeekBar.setVisibility(View.GONE);
                                            break;
                                        case 4:
                                            filter.remove(DAO.Type.RESTAURANT);
                                            break;
                                    }
                                }
                            }
                        })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        filterTypesDialog = builder.create();
    }

    @Override
    public void onClick(View view) {
        if (view == fromDateEtxt) {
            fromDatePickerDialog.show();
        } else if (view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        filter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
