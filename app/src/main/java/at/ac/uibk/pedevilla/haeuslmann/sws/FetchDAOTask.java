package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.util.List;
import java.util.Locale;

import mobi.seus.org.apache.http.HttpResponse;
import mobi.seus.org.apache.http.client.HttpClient;
import mobi.seus.org.apache.http.client.methods.HttpUriRequest;
import mobi.seus.org.apache.http.impl.client.BasicResponseHandler;
import mobi.seus.org.apache.http.impl.client.DefaultHttpClient;

public class FetchDAOTask extends AsyncTask<HttpUriRequest, Void, String>
{
    private static final String TAG = "ADAORestTask";
    public static final String HTTP_RESPONSE = "httpResponse";

    private Context mContext;
    private HttpClient mClient;
    private String mAction;
    private ProgressDialog dialog;

    public FetchDAOTask(Context context, String action)
    {
        mContext = context;
        mAction = action;
        dialog = new ProgressDialog(context);
        mClient = new DefaultHttpClient();
    }

    public FetchDAOTask(Context context, String action, HttpClient client)
    {
        mContext = context;
        mAction = action;
        dialog = new ProgressDialog(context);
        mClient = client;
    }

    @Override
    protected void onPreExecute() {
        dialog.setTitle("Loading");
        dialog.setMessage("Please be patient...");
        dialog.show();
    }

    @Override
    protected String doInBackground(HttpUriRequest... params)
    {
        try
        {
            HttpUriRequest request = params[0];
            HttpResponse serverResponse = mClient.execute(request);
            BasicResponseHandler handler = new BasicResponseHandler();
            String response = handler.handleResponse(serverResponse);
            DataParser dataParser = new DataParser(mContext, Locale.getDefault().getLanguage());
            response = response.replace("&nbsp;", " ");
            response = response.replace("&amp;", " ");
            dataParser.loadDataIntoModel(response);
            dataParser.parseAllData();
            List<DAO> dAOList = dataParser.getDaoList();
            DAO.setDetailsDAO(dAOList.get(0));
            return HTTP_RESPONSE;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response)
    {

        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        Intent intent = new Intent(mAction);
        intent.putExtra(HTTP_RESPONSE, response);
        // broadcast the completion
        mContext.sendBroadcast(intent);
    }

}