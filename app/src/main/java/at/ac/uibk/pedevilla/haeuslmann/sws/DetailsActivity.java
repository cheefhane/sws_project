package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.text.DateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class DetailsActivity extends AppCompatActivity {

    public static final String REST_ACTION = "THIS_IS_A_UNIQUE_KEY_WE_USE_TO_RETRIEVE";
    protected TextView nameTextView;
    protected TextView typeTextView;
    protected ImageView imageView;
    protected TextView captionTextView;
    protected TextView descriptionTextView;
    protected Button potentialActionButton;
    protected Button detailButton;
    private StringBuilder detailsStringBuilder;
    private DAO dao;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent myIntent = new Intent(DetailsActivity.this, DetailsActivity.class);
            DetailsActivity.this.startActivity(myIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String languageCode = Locale.getDefault().getLanguage();
        if (!languageCode.equals("nl") && !languageCode.equals("de") && !languageCode.equals("it")) {
            languageCode = "en";
        }

        nameTextView = (TextView) findViewById(R.id.titleTextView);
        typeTextView = (TextView) findViewById(R.id.typeTextView);
        imageView = (ImageView) findViewById(R.id.offerImageView);
        captionTextView = (TextView) findViewById(R.id.captionTextView);
        descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        potentialActionButton = (Button) findViewById(R.id.potentialActionButton);
        TextView detailsTextView = (TextView) findViewById(R.id.detailsTextView);
        detailButton = (Button) findViewById(R.id.detailButton);


        dao = DAO.getDetailsDAO();

        nameTextView.setText(dao.name);
        typeTextView.setText(dao.exactTypeName);

        if (isRealString(dao.description)) {
            descriptionTextView.setText(dao.description);
        } else {
            descriptionTextView.setVisibility(View.GONE);
        }

        Uri imageUri = dao.imageUri;
        if (imageUri == null) {
            imageView.setVisibility(View.GONE);
            captionTextView.setVisibility(View.GONE);
        } else if (!isRealString(dao.caption)) {
            Context imageContext = imageView.getContext();
            Picasso.with(imageContext).load(imageUri).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).fit().into(imageView);
            captionTextView.setVisibility(View.GONE);
        } else {
            Context imageContext = imageView.getContext();
            Picasso.with(imageContext).load(imageUri).into(imageView);
            captionTextView.setText(dao.caption);
        }

        LinearLayout buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);
        if (buttonLayout != null) {
            for (URL next : dao.urlSameAs) {
                Button sameAsButon = new Button(this);
                sameAsButon.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                sameAsButon.setText("Click here for more similar Infos");
                sameAsButon.setOnClickListener(new StartWebViewOnClickListener(next));
                buttonLayout.addView(sameAsButon);
            }

            for (URL url : dao.offeredByOrMakesOfferList) {
                Button sameAsButon = new Button(this);
                sameAsButon.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                if (dao.type == DAO.Type.OFFER) {
                    sameAsButon.setText("Click here to see details who offered this offer");
                } else {
                    sameAsButon.setText("Click here to see details of an offer");
                }
                sameAsButon.setOnClickListener(new ThisActivityChangeOnURLButtonListener(this, url, languageCode));
                buttonLayout.addView(sameAsButon);
            }

            if (dao.urlOfThisThing != null) {
                Button urlOfThisThing = new Button(this);
                urlOfThisThing.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                urlOfThisThing.setText("Link to this");
                urlOfThisThing.setOnClickListener(new StartWebViewOnClickListener(dao.urlOfThisThing));
                buttonLayout.addView(urlOfThisThing);
            }
        }


        //todo implement type specific information
        detailsStringBuilder = new StringBuilder();

        switch (dao.type) {
            case OFFER:
                showOfferUIDetails();
                break;
            case EVENT:
                break;
            case RESTAURANT:
                break;
            case TOURISTATTRACTION:
                break;
            case LODGINGBUSINESS:
                break;
        }

        showPotentialActionButtons();
        showRestDataMap();
        Spanned spanned = Html.fromHtml(detailsStringBuilder.toString());
        if (spanned != null && detailsTextView != null) {
            detailsTextView.setText(spanned);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.registerReceiver(receiver, new IntentFilter(REST_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(receiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void showRestDataMap() {
        Set<Map.Entry<String, String>> entries = dao.restDataMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            String label = entry.getKey();
            String value = entry.getValue();
            detailsStringBuilder.append("<br /><br />").append("<b>").append(label).append(":</b><br />").append(value);
        }
    }

    private void showPotentialActionButtons() {
        LinearLayout buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);


        Set<Map.Entry<String, URL>> entries = dao.potentialActionMap.entrySet();
        if (buttonLayout != null) {
            for (Map.Entry<String, URL> stringURLEntry : entries) {
                Button button = new Button(this);
                button.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                button.setText(stringURLEntry.getKey());
                button.setOnClickListener(new StartWebViewOnClickListener(stringURLEntry.getValue()));
                buttonLayout.addView(button);
            }
        }
    }

    private void showOfferUIDetails() {

        if (dao.price != 0 && isRealString(dao.priceCurrency)) {
            detailsStringBuilder.append("<b>Price: </b>").append(dao.price).append(" ").append(dao.priceCurrency);
            if (dao.priceValidFrom != null && dao.priceValidUntil != null) {
                DateFormat dateFormatter = android.text.format.DateFormat.getMediumDateFormat(getApplicationContext());
                String priceValidFrom = dateFormatter.format(dao.priceValidFrom.getTime());
                String priceValidUntil = dateFormatter.format(dao.priceValidUntil.getTime());
                detailsStringBuilder.append("<br /><b>Price valid from: </b>" + priceValidFrom + "<br /><b>Price valid until: </b>" + priceValidUntil);
            }
        }

        for (ItemOffered itemOffered : dao.itemsOffered) {
            String typeString = itemOffered.getTypeString();
            String nameStringInLanguage = itemOffered.getNameMap().get(dao.languageCode);
            detailsStringBuilder.append("<br /><b>Offering " + typeString + ": </b>" + nameStringInLanguage);
        }
    }

    private boolean isRealString(String string) {
        return string != null && !string.equals("");
    }
}
