package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.content.Context;
import android.net.Uri;
import android.os.StrictMode;

import com.github.jsonldjava.jena.JenaJSONLD;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Selector;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDF;

import org.apache.jena.riot.RDFDataMgr;

import java.io.InputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DataParser {
    public static final String HTTP_SCHEMA_ORG = "http://schema.org/";
    public static final String TYPE_PROPERTY_STRING = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    public static final String HTTP_SCHEMA_LABEL = "http://www.w3.org/2000/01/rdf-schema#label";

    private OntModel model;
    private List<DAO> daoList;
    private String languageCode;

    public DataParser(Context context, String languageCode) {
        this.languageCode = languageCode;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        final Model base = ModelFactory.createDefaultModel();
        model = ModelFactory.createOntologyModel(OntModelSpec.RDFS_MEM_RDFS_INF, base);

        // String url = "https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-deimos/data/releases/2.2/schema.nt";
        // InputStream inputStream = context.getResources().openRawResource(R.raw.schema);
        InputStream inputStream = context.getResources().openRawResource(R.raw.schema_min);
        model.read(inputStream, HTTP_SCHEMA_ORG, "N-TRIPLE");

        daoList = new ArrayList<>();
    }

    public void loadDataIntoModel(List<String> responseStrings) {
        for (String response : responseStrings) {
            StringReader reader = new StringReader(response);
            RDFDataMgr.read(model, reader, HTTP_SCHEMA_ORG, JenaJSONLD.JSONLD);
        }
    }

    public void loadDataIntoModel(String responseString) {
            StringReader reader = new StringReader(responseString);
            RDFDataMgr.read(model, reader, HTTP_SCHEMA_ORG, JenaJSONLD.JSONLD);
    }

    public void parseAllData() {

        parseDataForClass("http://schema.org/LodgingBusiness", DAO.Type.LODGINGBUSINESS);
        parseDataForClass("http://schema.org/Restaurant", DAO.Type.RESTAURANT);
        parseDataForClass("http://schema.org/TouristAttraction", DAO.Type.TOURISTATTRACTION);
        parseDataForClass("http://schema.org/Offer", DAO.Type.OFFER);
        parseDataForClass("http://schema.org/Event", DAO.Type.EVENT);
    }

    private void parseDataForClass(String className, DAO.Type type) {
        OntClass ontclass = model.getOntClass(className);

        //wanted Behavoiur that offers that a hotel makes are seperate offers !
        ExtendedIterator<? extends OntResource> instanceIterator = ontclass.listInstances();
        while (instanceIterator.hasNext()) {
            OntResource instanceResource = instanceIterator.next();

            parseDataFromResource(instanceResource, type);
        }
    }

    private void parseDataFromResource(OntResource instanceResource, DAO.Type type) {
        DAO dao = new DAO();
        dao.languageCode = languageCode;
        dao.type = type;

        parseThingSpecificData(instanceResource, dao);

        switch (type) {
            case OFFER:
                //is of types offer and thing
                parseOfferSpecificData(instanceResource, dao);
                break;
            case EVENT:
                //is of types event and thing
                parseEventSpecificData(instanceResource, dao);
                break;
            case RESTAURANT:
                //is of Foodestablishment, LocalBusiness, Place, Organisation and thing
                parseOrganizationSpecificData(instanceResource, dao);
                parsePlaceSpecificData(instanceResource, dao);
                parseRestaurantSpecificData(instanceResource, dao);
                break;
            case TOURISTATTRACTION:
                //is of types place and thing
                parsePlaceSpecificData(instanceResource, dao);
                parseTouristAttractionSpecificData(instanceResource, dao);
                break;
            case LODGINGBUSINESS:
                //is of types Localbusiness,Place, Organisation  and thing
                parsePlaceSpecificData(instanceResource, dao);
                parseLodgingBusinessSpecificData(instanceResource, dao);
                break;
        }


        parseRestData(instanceResource, dao);

        daoList.add(dao);
    }

    private void parseOrganizationSpecificData(OntResource instanceResource, DAO dao) {
        parseAddressData(instanceResource, dao);
        parseLocationData(instanceResource, dao);
    }

    private void parseLocationData(OntResource instanceResource, DAO dao) {
        Property locationProp = makeProp(HTTP_SCHEMA_ORG + "location");
        if (instanceResource.hasProperty(locationProp)) {
            Resource locationResource = instanceResource.getPropertyResourceValue(locationProp);

            if (locationResource.isLiteral()) {
                Literal literal = locationResource.asLiteral();
                String locationString = literal.getString();
                String propertyLabel = getPropertyLabel(locationProp);
                dao.restDataMap.put(propertyLabel, locationString);

            } else if (locationResource.isResource()) {

                Statement stmt = locationResource.getProperty(RDF.type);
                RDFNode object = stmt.getObject();
                String typeOfLocationResource = object.toString();
                if (isRealString(typeOfLocationResource) && typeOfLocationResource.equals(HTTP_SCHEMA_ORG + "PostalAddress")) {
                    getLabelAndStringValueAndAddToDaoRestMap(dao, locationResource, HTTP_SCHEMA_ORG + "locationCountry");
                    getLabelAndStringValueAndAddToDaoRestMap(dao, locationResource, HTTP_SCHEMA_ORG + "locationLocality");
                    getLabelAndStringValueAndAddToDaoRestMap(dao, locationResource, HTTP_SCHEMA_ORG + "locationRegion");
                    getLabelAndStringValueAndAddToDaoRestMap(dao, locationResource, HTTP_SCHEMA_ORG + "postOfficeBoxNumber");
                    getLabelAndStringValueAndAddToDaoRestMap(dao, locationResource, HTTP_SCHEMA_ORG + "postalCode");
                    getLabelAndStringValueAndAddToDaoRestMap(dao, locationResource, HTTP_SCHEMA_ORG + "streetAddress");
                } else if (isRealString(typeOfLocationResource) && typeOfLocationResource.equals(HTTP_SCHEMA_ORG + "Place")) {
                    Resource propertyResourceValue = locationResource.getPropertyResourceValue(makeProp(HTTP_SCHEMA_ORG + "address"));
                    if (propertyResourceValue != null) {
                        getLabelAndStringValueAndAddToDaoRestMap(dao, propertyResourceValue, HTTP_SCHEMA_ORG + "addressCountry");
                        getLabelAndStringValueAndAddToDaoRestMap(dao, propertyResourceValue, HTTP_SCHEMA_ORG + "addressLocality");
                        getLabelAndStringValueAndAddToDaoRestMap(dao, propertyResourceValue, HTTP_SCHEMA_ORG + "addressRegion");
                        getLabelAndStringValueAndAddToDaoRestMap(dao, propertyResourceValue, HTTP_SCHEMA_ORG + "postOfficeBoxNumber");
                        getLabelAndStringValueAndAddToDaoRestMap(dao, propertyResourceValue, HTTP_SCHEMA_ORG + "postalCode");
                        getLabelAndStringValueAndAddToDaoRestMap(dao, propertyResourceValue, HTTP_SCHEMA_ORG + "streetAddress");
                    }
                }
            }
        }
    }

    private void parsePlaceSpecificData(OntResource instanceResource, DAO dao) {
        parseAddressData(instanceResource, dao);

        //todo decide if enough
        //  parseMapData(instanceResource, dao);


    }


    private void parseAddressData(OntResource instanceResource, DAO dao) {
        Property addressProperty = makeProp(HTTP_SCHEMA_ORG + "address");
        if (instanceResource.hasProperty(addressProperty)) {

            Resource addressPropertyResource = instanceResource.getPropertyResourceValue(addressProperty);
            // TODO check not null
            if (addressPropertyResource != null && addressPropertyResource.isLiteral()) {
                Literal literal = addressPropertyResource.asLiteral();
                String addressString = literal.getString();
                String propertyLabel = getPropertyLabel(addressProperty);
                dao.restDataMap.put(propertyLabel, addressString);

            } else if (addressPropertyResource != null && addressPropertyResource.isResource()) {
                getLabelAndStringValueAndAddToDaoRestMap(dao, addressPropertyResource, HTTP_SCHEMA_ORG + "addressCountry");
                getLabelAndStringValueAndAddToDaoRestMap(dao, addressPropertyResource, HTTP_SCHEMA_ORG + "addressLocality");
                getLabelAndStringValueAndAddToDaoRestMap(dao, addressPropertyResource, HTTP_SCHEMA_ORG + "addressRegion");
                getLabelAndStringValueAndAddToDaoRestMap(dao, addressPropertyResource, HTTP_SCHEMA_ORG + "postOfficeBoxNumber");
                getLabelAndStringValueAndAddToDaoRestMap(dao, addressPropertyResource, HTTP_SCHEMA_ORG + "postalCode");
                getLabelAndStringValueAndAddToDaoRestMap(dao, addressPropertyResource, HTTP_SCHEMA_ORG + "streetAddress");
            }
        }
    }

    private void getLabelAndStringValueAndAddToDaoRestMap(DAO dao, Resource addressPropertyResource, String propString) {
        Property property = makeProp(propString);
        String string = getString(addressPropertyResource, property);
        String propertyLabel = getPropertyLabel(property);
        if (isRealString(string)) {
            dao.restDataMap.put(propertyLabel, string);
        }
    }

    private boolean isRealString(String string) {
        return string != null && !string.equals("");
    }

    private void parseRestData(OntResource instanceResource, DAO dao) {
        List<String> allreadyParsedStringsArray = new ArrayList<>();
        allreadyParsedStringsArray.add(TYPE_PROPERTY_STRING);
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "url");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "name");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "description");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "image");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "potentialAction");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "priceSpecification");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "address");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "sameAs");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "location");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "offeredBy");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "makesOffer");
        allreadyParsedStringsArray.add(HTTP_SCHEMA_ORG + "itemOffered");
        allreadyParsedStringsArray.add("http://purl.org/dc/terms/description");


        Map<String, String> restDataMap = new HashMap<>();
        StmtIterator iterator = instanceResource.listProperties();

        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            Property property = stmt.getPredicate();
            RDFNode objectNode = stmt.getObject();
            String propertyString = property.toString();
            if (!allreadyParsedStringsArray.contains(propertyString)) {

                String labelForProperty = getPropertyLabel(property);

                //todo make more beatiful and check for multi value properties , like teasers etc..
                if (objectNode.isLiteral()) {
                    Literal literal = objectNode.asLiteral();
                    String language = literal.getLanguage();
                    if (language == null || language.equals(languageCode) || language.equals("")) {
                        String objectString = literal.getString();
                        if (isRealString(objectString)) {
                            restDataMap.put(labelForProperty, objectString);
                        }
                    } else {
                        System.out.println("Restdata Literal in different Language: " + language + "Value: " + literal.getString());
                    }

                } else if (objectNode.isResource()) {
                    Resource res = objectNode.asResource();

                    //todo figure out what to do here
                    System.out.println("Unused Resource: Prop: " + labelForProperty + " Value: " + res.toString());
                }
            }
        }
        dao.restDataMap.putAll(restDataMap);
    }

    private String getPropertyLabel(Property property) {
        Statement stmtForLabel = property.getProperty(makeProp(HTTP_SCHEMA_LABEL));

        String labelForProperty;
        if (stmtForLabel != null) {

            RDFNode labelNode = stmtForLabel.getObject();
            if (labelNode.isLiteral()) {
                Literal labelLiteral = labelNode.asLiteral();
                labelForProperty = labelLiteral.getString();
            } else {
                labelForProperty = property.getLocalName();
            }
        } else {
            labelForProperty = property.getLocalName();
        }
        return labelForProperty;
    }

    private void parseThingSpecificData(OntResource instanceResource, DAO dao) {
        dao.name = getSingleLanguageStringValueOfProperty(instanceResource, "http://schema.org/name");
        dao.description = getSingleLanguageStringValueOfProperty(instanceResource, "http://schema.org/description");

        parsePotentialActionData(instanceResource, dao);

        Statement stmt = instanceResource.getProperty(makeProp("http://schema.org/url"));
        if (stmt != null) {
            RDFNode object = stmt.getObject();
            String urlOfThisThing = object.toString();

            if (isRealString(urlOfThisThing)) {
                try {
                    dao.urlOfThisThing = new URL(urlOfThisThing);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        List<URL> urls = new ArrayList<>();
        NodeIterator iterator = instanceResource.listPropertyValues(makeProp(HTTP_SCHEMA_ORG + "sameAs"));
        while (iterator.hasNext()) {
            RDFNode sameAsNode = iterator.nextNode();
            if (sameAsNode.isURIResource()) {
                String urlSameAs = sameAsNode.toString();
                if (isRealString(urlSameAs)) {
                    try {
                        urls.add(new URL(urlSameAs));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        dao.urlSameAs = urls;

        parseImageData(instanceResource, dao);

    }

    private String getExactType(OntResource resource, List<String> typeList, DAO.Type superType) {
        String exactType = null;
        for (String type : typeList) {
            exactType = getExactTypeString(resource, type);
        }
        if (!isRealString(exactType)) {
            exactType = superType.toString();
        }
        return exactType;
    }

    private String getExactTypeString(OntResource resource, String type) {
        OntClass ontClass = model.getOntClass(type);
        String exactTypeString = "";
        if (resource.hasRDFType(ontClass)) {
            exactTypeString = ontClass.toString();
            exactTypeString = exactTypeString.replace(HTTP_SCHEMA_ORG, "");
        }
        return exactTypeString;
    }

    private void parseOfferSpecificData(OntResource instanceResource, DAO dao) {
        List<String> subTypeList = Arrays.asList(HTTP_SCHEMA_ORG + "AggregateOffer");
        dao.exactTypeName = getExactType(instanceResource, subTypeList, DAO.Type.OFFER);

        parseOfferedByData(instanceResource, dao);


        parsePriceSpecificationData(instanceResource, dao);
        parseItemsOfferedData(instanceResource, dao);
        String offerValidFromString = getString(instanceResource, "http://schema.org/validFrom");
        if (isRealString(offerValidFromString)) {
            dao.offerValidFrom = getDateFromString(offerValidFromString);
        }

        String offerValidUntilString = getString(instanceResource, "http://schema.org/validThrough");
        if (isRealString(offerValidUntilString)) {
            dao.offerValidUntil = getDateFromString(offerValidUntilString);
        }

    }

    private void parseOfferedByData(OntResource instanceResource, DAO dao) {
        NodeIterator iterator = instanceResource.listPropertyValues(makeProp(HTTP_SCHEMA_ORG + "offeredBy"));
        while (iterator.hasNext()) {
            RDFNode rdfNode = iterator.nextNode();
            if (rdfNode.isURIResource()) {
                Resource resource = rdfNode.asResource();

                String string = resource.getURI();
                if (isRealString(string)) {
                    try {
                        URL url = new URL(string);
                        dao.offeredByOrMakesOfferList.add(url);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    private void parseItemsOfferedData(OntResource instanceResource, DAO dao) {
        Selector itemOfferSelector = makeSelectorFromPropAndSubject(instanceResource, "http://schema.org/itemOffered");
        List<Resource> itemOfferResourceList = getResourceList(itemOfferSelector);
        dao.itemsOffered = parseItemOfferList(itemOfferResourceList);
    }

    private void parsePriceSpecificationData(OntResource instanceResource, DAO dao) {

        Resource priceSpecificationResource = instanceResource.getPropertyResourceValue(makeProp("http://schema.org/priceSpecification"));
        if (priceSpecificationResource != null) {
            String priceString = getString(priceSpecificationResource, "http://schema.org/price");
            if (isRealString(priceString)) {
                try
                {
                    NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
                    dao.price = nf.parse(priceString).doubleValue();
                }
                catch (Exception e)
                {
                    System.out.println(e.getMessage());
                }
            }

            String priceValidFromString = getString(priceSpecificationResource, "http://schema.org/validFrom");
            if (isRealString(priceValidFromString)) {
                dao.priceValidFrom = getDateFromString(priceValidFromString);
            }

            String priceValidUntilString = getString(priceSpecificationResource, "http://schema.org/validThrough");
            if (isRealString(priceValidUntilString)) {
                dao.priceValidUntil = getDateFromString(priceValidUntilString);
            }

            dao.priceCurrency = getString(priceSpecificationResource, "http://schema.org/priceCurrency");

            String pricedTaxIncludedString = getString(priceSpecificationResource, "http://schema.org/valueAddedTaxIncluded");
            if (isRealString(pricedTaxIncludedString)) {
                dao.priceIncludesTaxes = Boolean.parseBoolean(pricedTaxIncludedString);
            }
        }
    }

    private void parseEventSpecificData(OntResource instanceResource, DAO dao) {
        List<String> subTypeList = Arrays.asList(HTTP_SCHEMA_ORG + "BusinessEvent", HTTP_SCHEMA_ORG + "ChildrensEvent", HTTP_SCHEMA_ORG + "ComedyEvent", HTTP_SCHEMA_ORG + "VisualArtsEvent", HTTP_SCHEMA_ORG + "SportsEvent", HTTP_SCHEMA_ORG + "SocialEvent", HTTP_SCHEMA_ORG + "ScreeningEvent", HTTP_SCHEMA_ORG + "PublicationEvent", HTTP_SCHEMA_ORG + "SaleEvent", HTTP_SCHEMA_ORG + "TheaterEvent", HTTP_SCHEMA_ORG + "MusicEvent", HTTP_SCHEMA_ORG + "LiteraryEvent", HTTP_SCHEMA_ORG + "FoodEvent", HTTP_SCHEMA_ORG + "Festival", HTTP_SCHEMA_ORG + "ExhibitionEvent", HTTP_SCHEMA_ORG + "EducationEvent", HTTP_SCHEMA_ORG + "DeliveryEvent", HTTP_SCHEMA_ORG + "DanceEvent");
        dao.exactTypeName = getExactType(instanceResource, subTypeList, DAO.Type.EVENT);
        parseLocationData(instanceResource, dao);
    }

    private void parseRestaurantSpecificData(OntResource instanceResource, DAO dao) {
        dao.exactTypeName = dao.type.toString();
        parseMakesOfferData(instanceResource, dao);
    }

    private void parseTouristAttractionSpecificData(OntResource instanceResource, DAO dao) {
        dao.exactTypeName = dao.type.toString();
    }

    private void parseMakesOfferData(OntResource instanceResource, DAO dao) {
        NodeIterator iterator = instanceResource.listPropertyValues(makeProp(HTTP_SCHEMA_ORG + "makesOffer"));
        while (iterator.hasNext()) {
            RDFNode rdfNode = iterator.nextNode();
            if (rdfNode.isURIResource()) {
                Resource resource = rdfNode.asResource();

                String string = resource.getURI();
                if (isRealString(string)) {
                    try {
                        URL url = new URL(string);
                        dao.offeredByOrMakesOfferList.add(url);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void parseLodgingBusinessSpecificData(OntResource instanceResource, DAO dao) {
        List<String> subTypeList = Arrays.asList(HTTP_SCHEMA_ORG + "BedAndBreakfast", HTTP_SCHEMA_ORG + "Hostel", HTTP_SCHEMA_ORG + "Hotel", HTTP_SCHEMA_ORG + "Motel");
        dao.exactTypeName = getExactType(instanceResource, subTypeList, DAO.Type.LODGINGBUSINESS);
        parseMakesOfferData(instanceResource, dao);
    }

    private void parsePotentialActionData(OntResource instanceResource, DAO dao) {

        Property potentialActionProperty = model.getProperty("http://schema.org/potentialAction");
        NodeIterator iterator = instanceResource.listPropertyValues(potentialActionProperty);
        Map<String, URL> potentialActionMap = new HashMap<>();

        while (iterator.hasNext()) {
            RDFNode potentialActionResourceNode = iterator.nextNode();
            if (potentialActionResourceNode.isResource()) {
                Resource potentialActionResource = potentialActionResourceNode.asResource();

                Property targetProperty = model.getProperty("http://schema.org/target");
                if (potentialActionResource.hasProperty(targetProperty)) {
                    String potentialActionTargetString = getString(potentialActionResource, targetProperty);
                    if (isRealString(potentialActionTargetString)) {
                        try {
                            URL potentialActionURL = new URL(potentialActionTargetString);

                            Statement stmt = potentialActionResource.getProperty(RDF.type);
                            if (stmt != null) {

                                RDFNode object = stmt.getObject();
                                Resource resource1 = object.asResource();
                                String potentialActionTypeString = resource1.getLocalName();
                                potentialActionMap.put(potentialActionTypeString, potentialActionURL);
                            }

                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        String buyActionString = getString(instanceResource, HTTP_SCHEMA_ORG + "BuyAction");
        if (isRealString(buyActionString)) {
            try {
                URL url = new URL(buyActionString);
                potentialActionMap.put("Buy", url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        dao.potentialActionMap = potentialActionMap;
    }


    private void parseImageData(OntResource instanceResource, DAO dao) {
        Property imageProperty = model.getProperty("http://schema.org/image");
        if (instanceResource.hasProperty(imageProperty)) {
            Resource imageResource = instanceResource.getPropertyResourceValue(imageProperty);
            String imageUrlString = getString(imageResource, "http://schema.org/contentUrl");
            if (isRealString(imageUrlString)) {
                dao.imageUri = Uri.parse(imageUrlString);
                // TODO not null check
            } else if (imageResource != null && imageResource.isLiteral()) {
                imageUrlString = imageResource.toString();
                if (isRealString(imageUrlString)) {
                    dao.imageUri = Uri.parse(imageUrlString);
                }
            }
            dao.caption = getSingleLanguageStringValueOfProperty(imageResource, "http://schema.org/caption");
        }
    }


    private List<String> getStringList(Selector selector) {
        List<String> stringList = new ArrayList<>();
        List<Literal> literalList = getLiteralList(selector);
        for (Literal literal : literalList) {
            stringList.add(literal.getString());
        }
        return stringList;
    }

    private String getString(Resource resource, String propertyString) {
        String result = null;
        Property property = model.getProperty(propertyString);
        //TODO remove null check
        if (resource != null && resource.hasProperty(property)) {
            result = getString(resource, property);
        }
        return result;
    }

    private String getString(Resource resource, Property property) {
        Selector selector = new SimpleSelector(resource, property, (RDFNode) null);
        return getString(selector);
    }

    private String getString(Selector selector) {
        List<String> stringList = getStringList(selector);
        String string = null;
        if (!stringList.isEmpty()) {
            string = stringList.get(0);
        }
        return string;
    }


    private String getSingleLanguageStringValueOfProperty(Resource instanceResource, String propertyNameString) {
        String stringValue = null;
        Property property = model.getProperty(propertyNameString);
        // TODO check not null
        if (instanceResource != null && instanceResource.hasProperty(property)) {
            Map<String, String> valueMap = getMulitLanguageStringMap(instanceResource, property);

            if (valueMap.containsKey(languageCode)) {
                stringValue = valueMap.get(languageCode);
            } else {
                stringValue = valueMap.get("");
            }
        }
        return stringValue;
    }


    private Map<String, String> getMulitLanguageStringMap(Resource resource, Property property) {
        Selector selector = new SimpleSelector(resource, property, (RDFNode) null);
        Map<String, String> stringMap = new HashMap<>();
        List<Literal> literalList = getLiteralList(selector);
        for (Literal literal : literalList) {
            String language = literal.getLanguage();
            String contentString = literal.getString();
            stringMap.put(language, contentString);
        }
        return stringMap;
    }


    private List<Literal> getLiteralList(Selector selector) {
        List<Literal> listeralList = new ArrayList<>();
        List<RDFNode> nodeList = getRDFNodeList(selector);
        for (RDFNode node : nodeList) {
            if (node.isLiteral()) {
                listeralList.add(node.asLiteral());
            }
        }
        return listeralList;
    }


    public List<RDFNode> getRDFNodeList(Selector selector) {
        List<RDFNode> rdfNodeList = new ArrayList<>();
        StmtIterator iterator = model.listStatements(selector);
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement();
            RDFNode node = stmt.getObject();
            rdfNodeList.add(node);
        }
        return rdfNodeList;
    }

    private List<ItemOffered> parseItemOfferList(List<Resource> itemOfferResourceList) {
        List<ItemOffered> itemsOfferedList = new ArrayList<>();
        for (Resource resource : itemOfferResourceList) {
            Selector nameSelector = makeSelectorFromPropAndSubject(resource, "http://schema.org/name");

            List<Literal> nameLiteralList = getLiteralList(nameSelector);
            Statement stmt = resource.getProperty(RDF.type);
            RDFNode object = stmt.getObject();
            Resource resource1 = object.asResource();
            String typeString = resource1.getLocalName();


            Map<String, String> nameMap = new HashMap<>();
            for (Literal nameLiteral : nameLiteralList) {
                String language = nameLiteral.getLanguage();
                if (!isRealString(language)) {
                    language = languageCode;
                }
                String name = nameLiteral.getString();
                nameMap.put(language, name);
            }
            ItemOffered itemOffered = new ItemOffered();
            itemOffered.setTypeString(typeString);
            itemOffered.setNameMap(nameMap);
            itemsOfferedList.add(itemOffered);
        }
        return itemsOfferedList;
    }


    public List<DAO> getDaoList() {
        return daoList;
    }

    private Date getDateFromString(String dateString) {
        String dateFormatString = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
        DateFormat dateformat = new SimpleDateFormat(dateFormatString);
        Date date = null;
        try {
            date = dateformat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private Property makeProp(String propertyString) {
        return model.getProperty(propertyString);
    }

    private SimpleSelector makeSelectorFromPropAndSubject(Resource subjectResource, String propertyString) {
        return new SimpleSelector(subjectResource, makeProp(propertyString), (RDFNode) null);
    }

    private List<Resource> getResourceList(Selector selector) {
        List<RDFNode> nodeList = getRDFNodeList(selector);
        return turnRDFNodeToResource(nodeList);
    }

    private List<Resource> turnRDFNodeToResource(List<RDFNode> rdfNodeList) {
        List<Resource> resourceList = new ArrayList<>();
        for (RDFNode node : rdfNodeList) {
            if (node.isResource()) {
                resourceList.add(node.asResource());
            }
        }
        return resourceList;
    }
}
