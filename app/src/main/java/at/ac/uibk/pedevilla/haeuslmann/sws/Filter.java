package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Filter implements LocationListener {
    public static final long MIN_TIME_BW_UPDATES = 600000;
    public static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1000;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static final String FETCH_LIMIT = "?limit=10";
    private static final String FETCH_RADIUS = "&radius=50";
    private final Activity context;
    ArrayList<DAO.Type> types;
    Date fromDate;
    Date toDate;
    double fromPrice;
    double toPrice;
    private double longitude;
    private double latitude;
    private Location location;
    private boolean isNetworkEnabled;
    private LocationManager manager;
    private SimpleDateFormat dateFormat;

    public Filter(Activity context) {
        this.context = context;

        getLocation2();
        this.types = new ArrayList<>();
        addDefaultTypes();
        this.fromDate = null;
        this.toDate = null;
        this.fromPrice = 0;
        this.toPrice = 500;
        this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    }

    private void getLocation2() {
        manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // getting GPS status
        boolean isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            boolean canGetLocation = true;
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                        // Show an expanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(context,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }

                    return;
                }
                manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                Log.d("activity", "LOC Network Enabled");
                if (manager != null) {
                    location = manager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        Log.d("activity", "LOC by Network");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        System.out.println("Long: " + longitude + " Lat: " + latitude);
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                                Manifest.permission.ACCESS_FINE_LOCATION)) {

                            // Show an expanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                        } else {

                            // No explanation needed, we can request the permission.

                            ActivityCompat.requestPermissions(context,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }

                        return;
                    }
                    manager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("activity", "RLOC: GPS Enabled");
                    if (manager != null) {
                        location = manager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Log.d("activity", "RLOC: loc by GPS");

                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    if (isNetworkEnabled) {
                        manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("activity", "LOC Network Enabled");
                        if (manager != null) {
                            location = manager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                Log.d("activity", "LOC by Network");
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    public ArrayList<String> getEndpoints() {
        ArrayList<String> endPoints = new ArrayList<>();
        String locationFilter = "";
        if (location != null) {
            locationFilter = "&lat=" + latitude + "&lon=" + longitude + FETCH_RADIUS;
            System.out.println("location filter: " + locationFilter);
        }
        for (DAO.Type type: types) {
            switch (type) {
                case EVENT:
                    String event = "events" + FETCH_LIMIT + locationFilter;
                    if (fromDate != null) {
                        event = event + "&startDate=" + dateFormat.format(fromDate);
                    }
                    if (toDate != null) {
                        event = event + "&endDate=" + dateFormat.format(toDate);
                    }
                    System.out.println("Event endpoint: " + event);
                    endPoints.add(event);
                    break;
                case TOURISTATTRACTION:
                    endPoints.add("attractions" + FETCH_LIMIT + locationFilter);
                    break;
                case OFFER:
                    endPoints.add("offers" + FETCH_LIMIT + "&maxprice=" + toPrice + "&minprice=" + fromPrice + locationFilter);
                    break;
                case LODGINGBUSINESS:
                    endPoints.add("businesses" + FETCH_LIMIT + "&type=LodgingBusiness" + locationFilter);
                    break;
                case RESTAURANT:
                    endPoints.add("businesses" + FETCH_LIMIT + "&type=Restaurant" + locationFilter);
                    break;
            }
        }
        return endPoints;
    }

    private void addDefaultTypes() {
        // types.add(DAO.Type.RESTAURANT);
        // types.add(DAO.Type.TOURISTATTRACTION);
        // types.add(DAO.Type.OFFER);
        types.add(DAO.Type.LODGINGBUSINESS);
        // types.add(DAO.Type.EVENT);
    }

    public ArrayList<DAO.Type> getTypes() {
        return types;
    }

    public void remove (DAO.Type type) {
        if (types.contains(type)) {
            types.remove(type);
        }
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public double getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(double fromPrice) {
        this.fromPrice = fromPrice;
    }

    public double getToPrice() {
        return toPrice;
    }

    public void setToPrice(double toPrice) {
        this.toPrice = toPrice;
    }


    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }
        System.out.println("Location changed to:  Lat: " + latitude + "  Long: " + longitude);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }
}
