package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.net.Uri;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAO implements Serializable {

    private static final long serialVersionUID = 1L;
    private static DAO daoToDisplayInfos;
    public Type type;
    public String exactTypeName;
    public String description;
    public String name;
    public String caption;
    public Uri imageUri;
    public Map<String, URL> potentialActionMap;
    public URL urlOfThisThing;
    public List<ItemOffered> itemsOffered;
    public double price;
    public String priceCurrency;
    public Date priceValidFrom;
    public Date priceValidUntil;
    public boolean priceIncludesTaxes;
    public String languageCode;
    public Date offerValidFrom;
    public Date offerValidUntil;
    public Map<String, String> restDataMap;
    public List<URL> urlSameAs;
    public List<URL> offeredByOrMakesOfferList;


    public DAO() {
        restDataMap = new HashMap<>();
        itemsOffered = new ArrayList<>();
        offeredByOrMakesOfferList = new ArrayList<>();
    }

    public static DAO getDetailsDAO() {

        return daoToDisplayInfos;
    }

    public static void setDetailsDAO(DAO detailsDAO) {
        daoToDisplayInfos = detailsDAO;
    }

    public enum Type {
        OFFER, EVENT, RESTAURANT, TOURISTATTRACTION, LODGINGBUSINESS
    }
}
