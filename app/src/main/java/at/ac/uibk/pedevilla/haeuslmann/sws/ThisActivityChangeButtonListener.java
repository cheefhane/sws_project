package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

public class ThisActivityChangeButtonListener implements View.OnClickListener {
    DAO dao;
    private Activity activity;

    public ThisActivityChangeButtonListener(Activity activity, DAO dao) {
        this.activity = activity;
        this.dao = dao;
    }

    @Override
    public void onClick(View view) {

        //very important in order to Acces it from the detailsActivity
        DAO.setDetailsDAO(dao);

        Intent myIntent = new Intent(activity, DetailsActivity.class);
        activity.startActivity(myIntent);
    }
}

