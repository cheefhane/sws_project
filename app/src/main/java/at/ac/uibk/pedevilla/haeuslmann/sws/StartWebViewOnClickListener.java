package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import java.net.URL;

public class StartWebViewOnClickListener implements View.OnClickListener {
    URL url;

    public StartWebViewOnClickListener(URL url) {
        this.url = url;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url.toString()));
        view.getContext().startActivity(intent);
    }
}