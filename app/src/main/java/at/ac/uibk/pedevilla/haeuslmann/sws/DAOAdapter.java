package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DAOAdapter extends RecyclerView.Adapter<DAOAdapter.DAOViewHolder> {
    private final MainActivity activity;
    private List<DAO> daoList;
    private Map<String, String> detailsButtonNameArray;

    public DAOAdapter(List<DAO> daoList, MainActivity mainActivity) {
        this.activity = mainActivity;
        this.daoList = daoList;
        detailsButtonNameArray = new HashMap<>();
        detailsButtonNameArray.put("en", "Show details");
        detailsButtonNameArray.put("de", "Details anzeigen");
        detailsButtonNameArray.put("it", "Showe las detailios");
    }


    @Override
    public DAOViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_offer, parent, false);

        return new DAOViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DAOViewHolder holder, int position) {
        holder.dao = daoList.get(position);
        showDaoDataInViewHolder(holder);
    }

    private void showDaoDataInViewHolder(DAOViewHolder holder) {
        DAO dao = holder.dao;
        holder.nameTextView.setVisibility(View.VISIBLE);
        holder.captionTextView.setVisibility(View.VISIBLE);
        holder.imageView.setVisibility(View.VISIBLE);
        holder.descriptionTextView.setVisibility(View.VISIBLE);
        holder.potentialActionButton.setVisibility(View.VISIBLE);
        holder.priceTextView.setVisibility(View.VISIBLE);




        holder.nameTextView.setText(dao.name);
        holder.typeTextView.setText(dao.exactTypeName);

        if (isRealString(dao.description)) {
            holder.descriptionTextView.setText(dao.description);
        } else {
            holder.descriptionTextView.setVisibility(View.GONE);
        }

        StringBuilder detailsStringBuilder = new StringBuilder();
        if (dao.price != 0 && isRealString(dao.priceCurrency)) {
            detailsStringBuilder.append("Price: " + dao.price + " " + dao.priceCurrency);
            if (dao.priceValidFrom != null && dao.priceValidUntil != null) {
                DateFormat dateFormatter = android.text.format.DateFormat.getMediumDateFormat(activity);
                String priceValidFrom = dateFormatter.format(dao.priceValidFrom.getTime());
                String priceValidUntil = dateFormatter.format(dao.priceValidUntil.getTime());
                detailsStringBuilder.append("\nPrice valid from: " + priceValidFrom + "\nPrice valid until: " + priceValidUntil);
            }
        } else {
            holder.priceTextView.setVisibility(View.GONE);
        }
        holder.priceTextView.setText(detailsStringBuilder);

        Uri imageUri = dao.imageUri;
        if (imageUri == null) {
            holder.imageView.setVisibility(View.GONE);
            holder.captionTextView.setVisibility(View.GONE);
        } else if (dao.caption == null || dao.caption.equals("")) {
            Picasso.with(activity).load(imageUri).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).fit().into(holder.imageView);
            holder.captionTextView.setVisibility(View.GONE);
        } else {
            Picasso.with(activity).load(imageUri).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).fit().into(holder.imageView);
            holder.captionTextView.setText(dao.caption);
        }
        if (dao.potentialActionMap.size() != 0) {
            Set<Map.Entry<String, URL>> entries = dao.potentialActionMap.entrySet();
            Iterator<Map.Entry<String, URL>> iterator = entries.iterator();
            Map.Entry<String, URL> next = iterator.next();
            URL targetURL = next.getValue();
            String type = next.getKey();

            holder.potentialActionButton.setOnClickListener(new StartWebViewOnClickListener(targetURL));
            holder.potentialActionButton.setText(type);

        } else {
            holder.potentialActionButton.setVisibility(View.GONE);
        }

        holder.detailButton.setText(detailsButtonNameArray.get(dao.languageCode));
        holder.detailButton.setOnClickListener(new ThisActivityChangeButtonListener(activity, dao));

        switch (dao.type) {
            case OFFER:

                break;
            case EVENT:
                break;
            case RESTAURANT:
                break;
            case TOURISTATTRACTION:
                break;
            case LODGINGBUSINESS:
                break;
        }
    }

    private boolean isRealString(String string) {
        return string != null && !string.equals("");
    }

    @Override
    public int getItemCount() {
        return daoList.size();
    }

    public static class DAOViewHolder extends RecyclerView.ViewHolder {

        protected final TextView nameTextView;
        protected final TextView typeTextView;
        protected final ImageView imageView;
        protected final TextView captionTextView;
        protected final TextView descriptionTextView;
        protected final Button potentialActionButton;
        protected final Button detailButton;
        protected final TextView priceTextView;
        protected DAO dao;

        public DAOViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            typeTextView = (TextView) itemView.findViewById(R.id.typeTextView);
            imageView = (ImageView) itemView.findViewById(R.id.offerImageView);
            captionTextView = (TextView) itemView.findViewById(R.id.captionTextView);
            descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            potentialActionButton = (Button) itemView.findViewById(R.id.potentialActionButton);
            detailButton = (Button) itemView.findViewById(R.id.detailButton);
        }
    }


}
