package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.app.Activity;
import android.view.View;

import java.net.URI;
import java.net.URL;

import mobi.seus.org.apache.http.client.methods.HttpGet;

public class ThisActivityChangeOnURLButtonListener implements View.OnClickListener {
    URL url;
    String languageCode;
    private Activity activity;

    public ThisActivityChangeOnURLButtonListener(Activity activity, URL url, String languageCode) {
        this.activity = activity;
        this.url = url;
        this.languageCode = languageCode;
    }

    @Override
    public void onClick(View view) {
        String urlString = url.toString();
        System.out.println("Rest url: " + urlString);
        urlString = urlString.replace("http://sws16.tr1k.de:8080/",MainActivity.REST_BASE_URL);
        try
        {
            HttpGet httpGet = new HttpGet(new URI(urlString));
            FetchDAOTask task = new FetchDAOTask(activity, DetailsActivity.REST_ACTION);
            task.execute(httpGet);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}

