package at.ac.uibk.pedevilla.haeuslmann.sws;


import java.util.HashMap;
import java.util.Map;

public class ItemOffered {
    private Map<String, String> nameMap = new HashMap<>();
    private String typeString = null;

    public Map<String, String> getNameMap() {
        return nameMap;
    }

    public void setNameMap(Map<String, String> nameMap) {
        this.nameMap = nameMap;
    }

    public String getTypeString() {
        return typeString;
    }

    public void setTypeString(String typeString) {
        this.typeString = typeString;
    }
}
