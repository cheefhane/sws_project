package at.ac.uibk.pedevilla.haeuslmann.sws;


import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Arrays;

import mobi.seus.org.apache.http.HttpResponse;
import mobi.seus.org.apache.http.client.HttpClient;
import mobi.seus.org.apache.http.client.methods.HttpUriRequest;
import mobi.seus.org.apache.http.impl.client.BasicResponseHandler;
import mobi.seus.org.apache.http.impl.client.DefaultHttpClient;

/**
 * Android RestTask (REST) from the Android Recipes book.
 */
public class DataParsingRestTask extends AsyncTask<HttpUriRequest, Void, String> {
    public static final String HTTP_RESPONSE = "httpResponse";
    private static final String TAG = "AARestTask";
    private MainActivity mainActivity;
    private HttpClient mClient;
    private String mAction;

    public DataParsingRestTask(MainActivity context, String action) {
        mainActivity = context;
        mAction = action;
        mClient = new DefaultHttpClient();
    }

    public DataParsingRestTask(MainActivity context, String action, HttpClient client) {
        mainActivity = context;
        mAction = action;
        mClient = client;
    }

    @Override
    protected void onPostExecute(String result) {
        Intent intent = new Intent(mAction);

        // broadcast the completion
        mainActivity.sendBroadcast(intent);
    }

    @Override
    protected String doInBackground(HttpUriRequest... params) {
        try {
            HttpUriRequest request = params[0];
            HttpResponse serverResponse = mClient.execute(request);
            BasicResponseHandler handler = new BasicResponseHandler();
            String response = handler.handleResponse(serverResponse);

            if (response != null) {
                Log.i("HTTP Response nummer " + mainActivity.intentCounter, response);
                response = response.replace("&nbsp;", " ");
                response = response.replace("&amp;", " ");
                mainActivity.dataParser.loadDataIntoModel(Arrays.asList(response));
            }

            mainActivity.intentCounter++;
            if (mainActivity.intentCounter == mainActivity.filter.getTypes().size()) {
                mainActivity.dataParser.parseAllData();
            }
            return HTTP_RESPONSE;
        } catch (Exception e) {
            // TODO handle this properly
            e.printStackTrace();
            return "";
        }
    }

}
