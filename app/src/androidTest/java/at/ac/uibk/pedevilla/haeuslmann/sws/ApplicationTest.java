package at.ac.uibk.pedevilla.haeuslmann.sws;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    // just a comment
    public ApplicationTest() {
        super(Application.class);
    }
}